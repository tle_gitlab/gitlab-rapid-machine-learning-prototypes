import Head from "next/head";
import { useState } from "react";
import styles from "./index.module.css";

export default function Label() {
  return (
    <div>
      <Head>
        <title>Gitlab AI Assisted - Label my merge request or issue</title>
      </Head>
      <main className={styles.main}>
        <img src="/tanuki.svg" className={styles.icon} />
        <a href="/label-merge-request">Label my merge request or issue</a>
        <a href="/explain-highlighted-code-block">Explain highlighted code block</a>
        <a href="/refactor-code">Refactor code</a>
        <a href="/fix-code">Fix code</a>
        <a href="/create-mermaid-chart">Create mermaid chart</a>
        <a href="/explain-postgres-query-plan">Explain postgres query plan</a>
      </main>
    </div>
  );
}
