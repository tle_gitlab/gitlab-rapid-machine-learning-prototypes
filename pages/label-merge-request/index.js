import Head from "next/head";
import { useState } from "react";
import styles from "./index.module.css";

export default function Label() {
  const defaultInput = `The chain of custody report was expanded from only merge commits to all commits in #267601 (closed)
There is an existing feature with this report where, in the UI below, you can provide a commit sha to generate a report only containing the commit with that sha.
`;
  const [mrInput, setMrInput] = useState(defaultInput);
  const [result, setResult] = useState();

  async function onSubmit(event) {
    event.preventDefault();
    try {
      const response = await fetch("/api/label", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ mr: mrInput }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw data.error || new Error(`Request failed with status ${response.status}`);
      }
      setResult(data.result);
    } catch(error) {
      console.error(error);
      alert(error.message);
    }
  }

  return (
    <div>
      <Head>
        <title>Gitlab AI Assisted - Label my merge request or issue</title>
      </Head>
      <main className={styles.main}>
      <a href="/">
         <img src="/tanuki.svg" className={styles.icon} />
      </a>
        <h3>Label my merge request or issue</h3>
        <form onSubmit={onSubmit}>
          <textarea
            name="mr"
            placeholder="Enter your merge request or issue description"
            rows = "5"
            value={mrInput}
            onChange={(e) => setMrInput(e.target.value)}
          />
          <input type="submit" value="Go" />
        </form>
      <div className={styles.result}><pre>/label ~{result}</pre></div>
      </main>
    </div>
  );
}
