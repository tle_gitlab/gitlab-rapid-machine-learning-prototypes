import Head from "next/head";
import { useState } from "react";
import styles from "./index.module.css";

export default function Explain() {
  const defaultInput = `const completion = await openai.createCompletion({
  model: "text-davinci-003",
  prompt: generatePrompt(req.body.animal),
  temperature: 0.6,
});
`;
  const [mrInput, setMrInput] = useState(defaultInput);
  const [result, setResult] = useState();

  async function onSubmit(event) {
    event.preventDefault();
    try {
      const response = await fetch("/api/explain", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ mr: mrInput }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw data.error || new Error(`Request failed with status ${response.status}`);
      }
      setResult(data.result);
    } catch(error) {
      console.error(error);
      alert(error.message);
    }
  }

  return (
    <div>
      <Head>
        <title>Gitlab AI Assisted - Explain highlighted code block</title>
      </Head>
      <main className={styles.main}>
      <a href="/">
        <img src="/tanuki.svg" className={styles.icon} />
      </a>
        <h3>Explain highlighted code block</h3>
        <form onSubmit={onSubmit}>
          <textarea
            name="mr"
            placeholder="Enter your merge request diff"
            rows = "5"
            value={mrInput}
            onChange={(e) => setMrInput(e.target.value)}
          />
          <input type="submit" value="Go" />
        </form>
      <div className={styles.result}>{result}</div>
      </main>
      </div>
  )
}
